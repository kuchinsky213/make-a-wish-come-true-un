package com.ggs.makeawishcometrueuniverse.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ggs.makeawishcometrueuniverse.MainActivity;
import com.ggs.makeawishcometrueuniverse.MakeWishFragment;
import com.ggs.makeawishcometrueuniverse.R;

public class Space extends AppCompatActivity {
TextView sended;
Button back;
ImageView cosmo;
    Animation animation,ses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space);
        back = findViewById(R.id.back);
sended = findViewById(R.id.sended);
cosmo = findViewById(R.id.cosmo);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fout);
        ses = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rer);



cosmo.startAnimation(animation);
sended.startAnimation(ses);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bb = new Intent(Space.this, MainActivity.class);
                startActivity(bb);
            }
        });
    }
}