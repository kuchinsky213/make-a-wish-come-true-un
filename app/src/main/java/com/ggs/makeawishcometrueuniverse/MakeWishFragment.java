package com.ggs.makeawishcometrueuniverse;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.ggs.makeawishcometrueuniverse.activity.NewProductActivity;
import com.ggs.makeawishcometrueuniverse.adapter.ProductListAdapter;
import com.ggs.makeawishcometrueuniverse.adapter.RecyclerItemClickListener;
import com.ggs.makeawishcometrueuniverse.entity.Product;
import com.ggs.makeawishcometrueuniverse.utils.ApplicationStrings;
import com.ggs.makeawishcometrueuniverse.viewmodel.ProductViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MakeWishFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MakeWishFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;
    public static final int EDIT_WORD_ACTIVITY_REQUEST_CODE = 2;

    private ProductViewModel mProductViewModel;
    private Button editButton, deleteButton;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MakeWishFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MakeWishFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MakeWishFragment newInstance(String param1, String param2) {
        MakeWishFragment fragment = new MakeWishFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_make_wish, container, false);


      //  Toolbar toolbar = view.findViewById(R.id.toolbar);

      //  ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
        final ProductListAdapter adapter = new ProductListAdapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        // Get a new or existing ViewModel from the ViewModelProvider.
        mProductViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mProductViewModel.getmAllProduct().observe(getActivity(), new Observer<List<Product>>() {
            @Override
            public void onChanged(@Nullable final List<Product> products) {
                // Update the cached copy of the words in the adapter.
                adapter.setmProducts(products);
            }
        });

        FloatingActionButton fab = view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NewProductActivity.class);
                startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE);
            }
        });


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, final int position) {
                        editButton = v.findViewById(R.id.button_edit);
                        //Toast.makeText(getApplicationContext(), "" + position, Toast.LENGTH_SHORT).show();
                        deleteButton = v.findViewById(R.id.button_delete);

                        deleteButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(getContext(), "Delete wish button is called "+adapter.getProductAt(position).getProductName() , Toast.LENGTH_SHORT).show();

                                mProductViewModel.delete(adapter.getProductAt(position));
                            }
                        });

                        editButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(getContext(), "Edit wish button is called "+adapter.getProductAt(position).getProductName() , Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), NewProductActivity.class);
                                intent.putExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_ID, adapter.getProductAt(position).getProductId());
                                intent.putExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_NAME, adapter.getProductAt(position).getProductName());
                                intent.putExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_DESCRIPTION, adapter.getProductAt(position).getProductDescription());
                                intent.putExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_QUANTITY,  Integer.toString(adapter.getProductAt(position).getProductQuantity()));
                                startActivityForResult(intent, EDIT_WORD_ACTIVITY_REQUEST_CODE);
                            }
                        });
                    }
                })
        );














        // Inflate the layout for this fragment
        return view;
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            String productName = data.getStringExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_NAME);
            String productDescription = data.getStringExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_DESCRIPTION);
            String productQuantity = data.getStringExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_QUANTITY);
            int quantity = Integer.parseInt(productQuantity);

            Product product = new Product(productName,productDescription, System.currentTimeMillis(),quantity,true);
            mProductViewModel.insert(product);
        } else if (requestCode == EDIT_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {

            int id = data.getIntExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_ID, -1);
            if (id == -1) {
                Toast.makeText(getContext(), "Note can't be updated", Toast.LENGTH_SHORT).show();
                return;
            }

            String productName = data.getStringExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_NAME);
            String productDescription = data.getStringExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_DESCRIPTION);
            String productQuantity = data.getStringExtra(ApplicationStrings.EXTRA_REPLY_PRODUCT_QUANTITY);
            int quantity = Integer.parseInt(productQuantity);

            Product product = new Product(productName,productDescription, System.currentTimeMillis(),quantity,true);
            product.setProductId(id);
            mProductViewModel.update(product);

            Toast.makeText(getActivity(), "Wish updated", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Wish not saved", Toast.LENGTH_SHORT).show();
        }
    }



}
