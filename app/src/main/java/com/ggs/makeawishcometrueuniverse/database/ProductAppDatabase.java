package com.ggs.makeawishcometrueuniverse.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.ggs.makeawishcometrueuniverse.dao.ProductDAO;
import com.ggs.makeawishcometrueuniverse.entity.Product;

@Database(entities = {Product.class}, version = 1)
public abstract  class ProductAppDatabase extends RoomDatabase {

    public abstract ProductDAO productDAO();

    private static volatile ProductAppDatabase INSTANCE;

    static ProductAppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ProductAppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ProductAppDatabase.class, "product_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
